<?php

$EM_CONF[$_EXTKEY] = [
    'title' => 'DIAM TWA Login',
    'description' => 'DIAM Two Way Authorization Login',
    'category' => 'fe',
    'version' => '1.0.0',
    'priority' => '',
    'module' => '',
    'state' => 'alpha',
    'uploadfolder' => false,
    'createDirs' => '',
    'modify_tables' => 'fe_users',
    'clearcacheonload' => true,
    'author' => 'A. Sauder',
    'author_email' => 'andre.sauder@viaduct.ch',
    'author_company' => 'Viaduct',
    'constraints' => [
        'depends' => [
            'typo3' => '7.6.0-7.99.99',
        ],
        'conflicts' => [],
        'suggests' => [],
    ]
];
