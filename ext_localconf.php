<?php

if (!defined('TYPO3_MODE')) {
    die ('Access denied.');
}

\TYPO3\CMS\Extbase\Utility\ExtensionUtility::configurePlugin(
    'DIAM: TWA Login',
    'diamtwalogin',
    [
        'Main' => 'list'
    ],
    [
        'Main' => 'list',
    ]
);
